import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by saranshsharma on 12/27/15.
 */
public class CheckDistance
{
    public static ArrayList<Integer> inputList = new ArrayList<Integer>();
    public static String[] inputLabels = {"X1", "Y1", "X2", "Y2", "N"};

    public static void main(String[] args) {

        System.out.println("This will ask for a set of 5 values. \nFirst 4 will be 2 pairs of cartesian coordinate points. \nThe last value will be the max number of moves allowed");
        fetchInput();
        int movesCalculated = checkMovesNeeded();
        int maxMovesAllowed = inputList.get(4);

        if (maxMovesAllowed >= movesCalculated) {
            System.out.println("\n*---- ----- ----- ----- -----*");
            System.out.println("(X1,Y1) = (" + inputList.get(0) + "," + inputList.get(1) + ")");
            System.out.println("(X2,Y2) = (" + inputList.get(2) + "," + inputList.get(3) + ")");
            System.out.println("Yes. " + "Need a min of " + movesCalculated + " to get to target. Given moves " + maxMovesAllowed);
        } else
            System.out.println("No. " + maxMovesAllowed + " isn't enough moves to get to point. Need " + movesCalculated + " moves or more");

    }

    public static int checkMovesNeeded() {

        int X1 = inputList.get(0);
        int Y1 = inputList.get(1);
        int X2 = inputList.get(2);
        int Y2 = inputList.get(3);

        int diagMoves = 0;

        while ((X1 != X2) || (Y1 != Y2)) {

            if (X1 == X2) break;
            if (X1 > X2) {
                X1 = X1 - 1;
            } else {
                X1 = X1 + 1;
            }

            if (Y1 == Y2) break;
            if (Y1 > Y2) {
                Y1 = Y1 - 1;
            } else {
                Y1 = Y1 + 1;
            }
            diagMoves++;
        }

        System.out.println("Total diag moves: " + diagMoves);
        System.out.println("X1,Y1 =" + X1 + "," + Y1);
        System.out.println("X2,Y2 =" + X2 + "," + Y2);
        if ((X1 == X2) && (Y1 == Y2)) {
            return diagMoves;
        } else {

            int straightMoves = Math.abs(X1 - X2) + Math.abs(Y1 - Y2);
            System.out.println("Total straight moves: "+straightMoves);
            return diagMoves + straightMoves;
        }

    }

    public static void fetchInput() {

        for (String item : inputLabels) {
            System.out.print("Enter " + item + ": ");
            inputList.add(readInput());
        }
    }

    public static int readInput() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int i = 0;
        int maxRetries = 5;

        for (int j = 0; j < maxRetries; j++) {

            try {
                i = Integer.parseInt(br.readLine());
                System.out.println("You entered: " + i + "\n");
                break;
            } catch (NumberFormatException nfe) {
                System.err.println("Input needs to be a number. Try again");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return i;
    }
}
